package edu.uw.tcss450.phishapp.show;

import java.io.Serializable;

public class Show implements Serializable {

    private final String mShowDate;
    private final String mLongDate;
    private final String mUrl;
    private final String mArtist;
    private final String mVenue;
    private final String mLocation;
    private final String mData;
    private final String mNotes;

    public static class Builder {
        private String mLongDate;
        private String mVenue;
        private String mArtist;
        private String mShowDate = "";
        private String mUrl = "";
        private String mLocation = "";
        private String mData = "";
        private String mNotes = "";

        public Builder(String longDate, String venue, String artist) {
            this.mLongDate = longDate;
            this.mVenue = venue;
            this.mArtist = artist;
        }

        public Builder addShowDate(final String val) {
            mShowDate = val;
            return this;
        }

        public Builder addUrl(final String val) {
            mUrl = val;
            return this;
        }

        public Builder addLocation(final String val) {
            mLocation = val;
            return this;
        }

        public Builder addData(final String val) {
            mData = val;
            return this;
        }

        public Builder addNotes(final String val) {
            mNotes = val;
            return this;
        }

        public Show build() {
            return new Show(this);
        }

    }

    private Show(final Builder builder) {
        mLongDate = builder.mLongDate;
        mShowDate = builder.mShowDate;
        mUrl = builder.mUrl;
        mArtist = builder.mArtist;
        mVenue = builder.mVenue;
        mLocation = builder.mLocation;
        mData = builder.mData;
        mNotes = builder.mNotes;
    }

    public String getLongDate() {
        return mLongDate;
    }

    public String getShowDate() {
        return mShowDate;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getArtist() {
        return mArtist;
    }

    public String getVenue() {
        return mVenue;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getData() {
        return mData;
    }

    public String getNotes() {
        return mNotes;
    }
}
