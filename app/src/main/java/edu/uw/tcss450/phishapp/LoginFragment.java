package edu.uw.tcss450.phishapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import edu.uw.tcss450.phishapp.model.Credentials;
import edu.uw.tcss450.phishapp.utils.SendPostAsyncTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnLoginFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class LoginFragment extends Fragment implements OnClickListener {

    private OnLoginFragmentInteractionListener mListener;
    private Credentials mCredentials;
    private String mFirebaseToken;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences prefs =
                getActivity().getSharedPreferences(
                        getString(R.string.keys_shared_prefs),
                        Context.MODE_PRIVATE);
        //retrieve the stored credentials from SharedPrefs
        if (prefs.contains(getString(R.string.keys_prefs_email)) &&
                prefs.contains(getString(R.string.keys_prefs_password))) {
            final String email = prefs.getString(getString(R.string.keys_prefs_email), "");
            final String password = prefs.getString(getString(R.string.keys_prefs_password), "");
            //Load the two login EditTexts with the credentials found in SharedPrefs
            EditText emailEdit = getActivity().findViewById(R.id.login_username);
            emailEdit.setText(email);
            EditText passwordEdit = getActivity().findViewById(R.id.login_password);
            passwordEdit.setText(password);

            if (!email.isEmpty() && !password.isEmpty()) {
                attemptLogin();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        Button register = v.findViewById(R.id.register_start_button);
        register.setOnClickListener(this);

        Button login = v.findViewById(R.id.login_button);
        login.setOnClickListener(this);

        return v;
    }

    private void saveCredentials(final Credentials credentials) {
        SharedPreferences prefs =
                getActivity().getSharedPreferences(
                        getString(R.string.keys_shared_prefs),
                        Context.MODE_PRIVATE);
        //Store the credentials in SharedPrefs
        prefs.edit().putString(getString(R.string.keys_prefs_email), credentials.getEmail()).apply();
        prefs.edit().putString(getString(R.string.keys_prefs_password), credentials.getPassword()).apply();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            switch (v.getId()) {
                case R.id.login_button:
                    attemptLogin();
                    break;
                case R.id.register_start_button:
                    mListener.onRegisterNewAccount();
                    break;
                default:
                    throw new IllegalStateException("Not implemented");
            }
        }
    }

    private void getFirebaseToken(final String email, final String password) {
        mListener.onWaitFragmentInteractionShow();
        //add this app on this device to listen for the topic all
        FirebaseMessaging.getInstance().subscribeToTopic("all");
        //the call to getInstanceId happens asynchronously. task is an onCompleteListener
        //similar to a promise in JS.
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("FCM: ", "getInstanceId failed", task.getException());
                        mListener.onWaitFragmentInteractionHide();
                        return;
                    }
                    // Get new Instance ID token
                    mFirebaseToken = task.getResult().getToken();
                    Log.d("FCM: ", mFirebaseToken);
                    //the helper method that initiates login service
                    doLogin(email, password);
                });
        //no code here. wait for the Task to complete.
    }

    private void doLogin(String email, String password) {
        Credentials credentials = new Credentials.Builder(
                email.trim(),
                password.trim())
                .build();

        Uri uri = new Uri.Builder()
                .scheme("https")
                .appendEncodedPath(getString(R.string.ep_base_url))
                .appendEncodedPath(getString(R.string.ep_login))
                .appendPath(getString(R.string.ep_with_token))
                .build();

        JSONObject msg = credentials.asJSONObject();

        try {
            msg.put("token", mFirebaseToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mCredentials = credentials;

        new SendPostAsyncTask.Builder(uri.toString(), msg)
                .onPreExecute(this::handleLoginOnPre)
                .onPostExecute(this::handleLoginOnPost)
                .onCancelled(this::handleErrorsInTask)
                .build().execute();
    }

    private void attemptLogin() {
        EditText usernameEditText = getActivity().findViewById(R.id.login_username);
        EditText passwordEditText = getActivity().findViewById(R.id.login_password);

        if (validateCredentials(usernameEditText, passwordEditText)) {
            getFirebaseToken(usernameEditText.getText().toString(),
                    passwordEditText.getText().toString());
        }
    }

    private boolean validateCredentials(EditText usernameText, EditText passwordEditText) {
        boolean valid = true;

        if (usernameText != null && passwordEditText != null) {
            String username = usernameText.getText().toString();
            String password = passwordEditText.getText().toString();

            if (username.isEmpty()) {
                valid = false;
                usernameText.setError("Field must not be empty.");
            }

            if (password.isEmpty()) {
                valid = false;
                passwordEditText.setError("Field must not be empty.");
            } else if (password.length() < 6) {
                valid = false;
                passwordEditText.setError("Field must be 6 characters or longer.");
            }
        } else {
            valid = false;
        }

        return valid;
    }

    /**
     * Handle errors that may occur during the AsyncTask.
     *
     * @param result the error message provide from the AsyncTask
     */
    private void handleErrorsInTask(String result) {
        Log.e("ASYNC_TASK_ERROR", result);
    }

    /**
     * Handle the setup of the UI before the HTTP call to the webservice.
     */
    private void handleLoginOnPre() {
    }

    /**
     * Handle onPostExecute of the AsynceTask. The result from our webservice is
     * a JSON formatted String. Parse it for success or failure.
     *
     * @param result the JSON formatted String response from the web service
     */
    private void handleLoginOnPost(String result) {
        try {
            Log.d("JSON result", result);
            JSONObject resultsJSON = new JSONObject(result);
            boolean success = resultsJSON.getBoolean("success");
            String token = resultsJSON.getString("token");
            mListener.onWaitFragmentInteractionHide();
            if (success) {
                //Login was successful. Inform the Activity so it can do its thing.
                saveCredentials(mCredentials);
                mListener.onLogin(mCredentials, token);
            } else {
                //Login was unsuccessful. Don’t switch fragments and inform the user
                ((TextView) getView().findViewById(R.id.login_username))
                        .setError("Login Unsuccessful");
            }
        } catch (JSONException e) {
            //It appears that the web service didn’t return a JSON formatted String
            //or it didn’t have what we expected in it.
            Log.e("JSON_PARSE_ERROR", result
                    + System.lineSeparator()
                    + e.getMessage());
            mListener.onWaitFragmentInteractionHide();
            ((TextView) getView().findViewById(R.id.login_username))
                    .setError("Login Unsuccessful");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnLoginFragmentInteractionListener extends WaitFragment.OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onLogin(final Credentials credentials, final String jwToken);

        void onRegisterNewAccount();
    }
}
