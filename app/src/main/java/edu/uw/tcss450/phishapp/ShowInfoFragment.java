package edu.uw.tcss450.phishapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class ShowInfoFragment extends Fragment implements OnClickListener {

    private OnFragmentInteractionListener mListener;
    private String url;

    public ShowInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onFragmentInteraction(url);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_show_info, container, false);

        if (getArguments() != null) {
            Bundle   args          = getArguments();
            TextView longDateTextView = v.findViewById(R.id.longDate);
            TextView locationTextView = v.findViewById(R.id.showLocation);
            TextView dataTextView = v.findViewById(R.id.showData);
            TextView notesTextView = v.findViewById(R.id.showNotes);

            if (longDateTextView != null) {
                longDateTextView.setText(args.getString("date"));
            }

            if (locationTextView != null) {
                locationTextView.setText(args.getString("location"));
            }

            if (dataTextView != null) {
                dataTextView.setText(Html.fromHtml(args.getString("data"), Html.FROM_HTML_MODE_COMPACT));
            }

            if (notesTextView != null) {
                notesTextView.setText(Html.fromHtml(args.getString("notes"), Html.FROM_HTML_MODE_COMPACT));
            }

            url = args.getString("url");
        }

        Button button = v.findViewById(R.id.fullShowDetailsButton);
        button.setOnClickListener(this);

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String url);
    }
}
