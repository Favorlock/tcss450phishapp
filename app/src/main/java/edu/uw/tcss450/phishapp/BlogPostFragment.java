package edu.uw.tcss450.phishapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlogPostFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BlogPostFragment extends Fragment implements OnClickListener {

    private OnFragmentInteractionListener mListener;
    private String url;

    public BlogPostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onFragmentInteraction(url);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                                               + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_blog_post, container, false);

        if (getArguments() != null) {
            Bundle   args          = getArguments();
            TextView dateTextView = v.findViewById(R.id.blogDate);
            TextView titleTextView = v.findViewById(R.id.blogTitle);
            TextView teaserTextView = v.findViewById(R.id.blogTeaser);

            if (dateTextView != null) {
                dateTextView.setText(args.getString("date"));
            }

            if (titleTextView != null) {
                titleTextView.setText(args.getString("title"));
            }

            if (teaserTextView != null) {
                teaserTextView.setText(Html.fromHtml(args.getString("teaser"), Html.FROM_HTML_MODE_COMPACT));
            }

            url = args.getString("url");
        }

        Button button = v.findViewById(R.id.fullPostButton);
        button.setOnClickListener(this);

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String url);
    }
}
