package edu.uw.tcss450.phishapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import edu.uw.tcss450.phishapp.LoginFragment.OnLoginFragmentInteractionListener;
import edu.uw.tcss450.phishapp.RegistrationFragment.OnRegistrationFragmentInteractionListener;
import edu.uw.tcss450.phishapp.model.Credentials;

public class MainActivity extends AppCompatActivity implements OnLoginFragmentInteractionListener, OnRegistrationFragmentInteractionListener {

    private boolean mLoadFromChatNotification = false;
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("type")) {
                Log.d(TAG, "type of message: " + getIntent().getExtras().getString("type"));
                mLoadFromChatNotification = getIntent().getExtras().getString("type").equals("msg");
            } else {
                Log.d(TAG, "NO MESSAGE");
            }
        }

        if (savedInstanceState == null) {
            if (findViewById(R.id.frame_main_container) != null) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.frame_main_container, new LoginFragment())
                        .commit();
            }
        }
    }

    @Override
    public void onLogin(Credentials credentials, final String jwToken) {
        Log.d("MainActivity", "onLogin");

        if (credentials != null) {
            successfulLogin(credentials, jwToken);
        }
    }

    @Override
    public void onRegisterNewAccount() {
        Log.d("MainActivity", "onRegisterNewAccount");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_main_container, new RegistrationFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRegister(Credentials credentials) {
        Log.d("MainActivity", "onRegister");

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void successfulLogin(Credentials credentials, final String jwToken) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("credentials", credentials);
        intent.putExtra(getString(R.string.keys_intent_notifification_msg), mLoadFromChatNotification);
        intent.putExtra(getString(R.string.keys_intent_jwToken), jwToken);
        startActivity(intent);
        //End this Activity and remove it from the Activity back stack.
        finish();
    }

    @Override
    public void onWaitFragmentInteractionShow() {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_main_container, new WaitFragment(), "WAIT")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onWaitFragmentInteractionHide() {
        getSupportFragmentManager()
                .beginTransaction()
                .remove(getSupportFragmentManager().findFragmentByTag("WAIT"))
                .commit();
    }
}
