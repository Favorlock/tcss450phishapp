package edu.uw.tcss450.phishapp;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import edu.uw.tcss450.phishapp.LoginFragment.OnLoginFragmentInteractionListener;
import edu.uw.tcss450.phishapp.model.Credentials;
import edu.uw.tcss450.phishapp.utils.SendPostAsyncTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements OnClickListener {

    private OnRegistrationFragmentInteractionListener mListener;
    private Credentials mCredentials;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnRegistrationFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegistrationFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_registration, container, false);

        Button button = v.findViewById(R.id.registration_confirm_button);
        button.setOnClickListener(this);

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            switch (v.getId()) {
                case R.id.registration_confirm_button:
                    attemptRegister();
                    break;
                default:
                    throw new IllegalStateException("Not implemented");
            }
        }
    }

    private void attemptRegister() {
        EditText usernameEditText = getActivity().findViewById(R.id.registration_username);
        EditText firstNameEditText = getActivity().findViewById(R.id.registration_first_name);
        EditText lastNameEditText = getActivity().findViewById(R.id.registration_last_name);
        EditText emailEditText = getActivity().findViewById(R.id.registration_email);
        EditText passwordEditText = getActivity().findViewById(R.id.registration_password);
        EditText passwordConfirmEditText = getActivity().findViewById(R.id.registration_password_confirm);

        if (validateCredentials(usernameEditText,
                firstNameEditText,
                lastNameEditText,
                emailEditText,
                passwordEditText,
                passwordConfirmEditText)) {
            Credentials credentials = new Credentials.Builder(
                    emailEditText.getText().toString(),
                    passwordEditText.getText().toString())
                    .addUsername(usernameEditText.getText().toString())
                    .addFirstName(firstNameEditText.getText().toString())
                    .addLastName(lastNameEditText.getText().toString())
                    .build();

            Uri uri = new Uri.Builder()
                    .scheme("https")
                    .appendEncodedPath(getString(R.string.ep_base_url))
                    .appendEncodedPath(getString(R.string.ep_register))
                    .build();

            JSONObject msg = credentials.asJSONObject();

            mCredentials = credentials;

            new SendPostAsyncTask.Builder(uri.toString(), msg)
                    .onPreExecute(this::handleLoginOnPre)
                    .onPostExecute(this::handleLoginOnPost)
                    .onCancelled(this::handleErrorsInTask)
                    .build().execute();
        }
    }

    private boolean validateCredentials(EditText usernameEditText,
                                        EditText firstNameEditText,
                                        EditText lastNameEditText,
                                        EditText emailEditText,
                                        EditText passwordEditText,
                                        EditText passwordConfirmEditText) {
        boolean valid = true;

        if (emailEditText != null && passwordEditText != null) {
            String username = usernameEditText.getText().toString();
            String firstName = firstNameEditText.getText().toString();
            String lastName = lastNameEditText.getText().toString();
            String email = emailEditText.getText().toString();
            String password = passwordEditText.getText().toString();
            String passwordConfirm = passwordConfirmEditText != null ? passwordConfirmEditText.getText().toString() : null;

            if (username.isEmpty()) {
                valid = false;
                usernameEditText.setError("Email must not be empty");
            }

            if (firstName.isEmpty()) {
                valid = false;
                firstNameEditText.setError("Email must not be empty");
            }

            if (lastName.isEmpty()) {
                valid = false;
                lastNameEditText.setError("Email must not be empty");
            }

            if (email.isEmpty()) {
                valid = false;
                emailEditText.setError("Email must not be empty");
            } else if (email.split("@").length != 2) {
                valid = false;
                emailEditText.setError("Invalid email, must include @");
            }

            if (password.isEmpty()) {
                valid = false;
                passwordEditText.setError("Password must not be empty");
            } else if (password.length() < 6) {
                valid = false;
                passwordEditText.setError("Password must be at least 6 characters");
            } else if (passwordConfirm != null && !password.equals(passwordConfirm)) {
                valid = false;
                passwordConfirmEditText.setError("Passwords do not match");
            }
        } else {
            valid = false;
        }

        return valid;
    }

    /**
     * Handle errors that may occur during the AsyncTask.
     *
     * @param result the error message provide from the AsyncTask
     */
    private void handleErrorsInTask(String result) {
        Log.e("ASYNC_TASK_ERROR", result);
    }

    /**
     * Handle the setup of the UI before the HTTP call to the webservice.
     */
    private void handleLoginOnPre() {
        mListener.onWaitFragmentInteractionShow();
    }

    /**
     * Handle onPostExecute of the AsynceTask. The result from our webservice is
     * a JSON formatted String. Parse it for success or failure.
     *
     * @param result the JSON formatted String response from the web service
     */
    private void handleLoginOnPost(String result) {
        try {
            Log.d("JSON result", result);
            JSONObject resultsJSON = new JSONObject(result);
            boolean success = resultsJSON.getBoolean("success");
            mListener.onWaitFragmentInteractionHide();
            if (success) {
                //Login was successful. Inform the Activity so it can do its thing.
                mListener.onRegister(mCredentials);
            } else {
                //Login was unsuccessful. Don’t switch fragments and inform the user
                ((TextView) getView().findViewById(R.id.registration_email))
                        .setError("Login Unsuccessful");
            }
        } catch (JSONException e) {
            //It appears that the web service didn’t return a JSON formatted String
            //or it didn’t have what we expected in it.
            Log.e("JSON_PARSE_ERROR", result
                    + System.lineSeparator()
                    + e.getMessage());
            mListener.onWaitFragmentInteractionHide();
            ((TextView) getView().findViewById(R.id.registration_email))
                    .setError("Login Unsuccessful");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnRegistrationFragmentInteractionListener extends WaitFragment.OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onRegister(Credentials credentials);
    }

}
